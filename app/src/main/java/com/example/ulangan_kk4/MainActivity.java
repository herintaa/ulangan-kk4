package com.example.ulangan_kk4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText et_panjang, et_lebar, et_tinggi;
    private double p, l, t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_panjang = findViewById(R.id.etPanjang);
        et_lebar = findViewById(R.id.etLebar);
        et_tinggi = findViewById(R.id.etTinggi);
    }

    public void data(List<String> variabel){
        String panjang = et_panjang.getText().toString();
        String lebar = et_lebar.getText().toString();
        String tinggi = et_tinggi.getText().toString();

        p = Double.parseDouble(panjang);
        l = Double.parseDouble(lebar);
        t = Double.parseDouble(tinggi);
    }

    public void hitung_keliling(View view) {
        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double keliling = 4*(p + l + t);

        Intent intent = new Intent(this, result.class);
        intent.putExtra("hasil", String.valueOf("Keliling dari balok adalah"));
        intent.putExtra("data", String.valueOf(keliling));


        startActivity(intent);
    }

    public void hitung_luas(View view) {
        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double luas = p*l;

        Intent intent = new Intent(this, result.class);
        intent.putExtra("hasil", String.valueOf("Luas dari balok adalah"));
        intent.putExtra("data", String.valueOf(luas));

        startActivity(intent);
    }

    public void hitung_volum(View view) {
        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double volum = p*l*t;

        Intent intent = new Intent(MainActivity.this, result.class);
        intent.putExtra("hasil", String.valueOf("Volume dari balok adalah"));
        intent.putExtra("data", String.valueOf(volum));

        startActivity(intent);
    }


}
