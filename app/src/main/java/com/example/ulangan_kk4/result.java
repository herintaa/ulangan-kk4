package com.example.ulangan_kk4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class result extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();

        String result = intent.getStringExtra("data");
        TextView tv = findViewById(R.id.tvResult);
        tv.setText(result);
    }
}
